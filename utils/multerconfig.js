var multer = require('multer');
var unirest = require('unirest');

var storage = multer.diskStorage({
  destination: function(req, file, next){
    next(null, './public/images');
  },    
  filename: function(req, file, next){
    const ext = file.mimetype.split('/')[1];
    next(null, file.fieldname + '-' + Date.now() + '.'+ext);
  } 
});

var fileFilter = function(req, file, next){
  const image = file.mimetype.startsWith('image/');
  if(image){
    next(null, true); 
    /*var ext = file.mimetype.split('/')[1];
    var filename = file.fieldname + '-' + Date.now() + '.'+ext; //console.log(filename);
    unirest.post("https://apicloud-facerect.p.mashape.com/process-file.json")
    .header("X-Mashape-Key", "5ZQmvCyI7LmshtjucnWQcHltmXJpp1RejJvjsnFezLUBkQJQqg")
    .header("X-Mashape-Host", "apicloud-facerect.p.mashape.com")
    .header("Content-Type", "multipart/form-data")
    .attach('imagej', './public/photo-storage/'+filename, {
      contentType: 'application/octet-stream',
      knownLength: 1000000
    })
    .end(function (result) {
      //console.log(result.status, result.headers, result.body);
    });*/
  }else{
    next(new Error("Veuillez fournir une image") , false);
  }
}

var upload = multer({ 
  storage: storage, 
  fileFilter: fileFilter
}).single('photo');

module.exports = upload;



