var multer = require('multer');
var upload = require('../utils/multerconfig.js');

var appRouter = function (app) {

  	app.post('/upload', function(req,res,err) {
        upload(req, res, function(err) {
         if (err) {
            return res.json({
                status: 400,
                message: err.message
            });
         }else{
         	return res.json({
                status: 200,
                message: 'Success'
            });
         }
         //
     	});
  	});
}

module.exports = appRouter;